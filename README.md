# Voidy

Depedency Intaller for Void Linux Fresh Intall

```
$ voidy -h
 Usage: voidy [OPTIONS]

 OPTIONS
     -h,--help              Display this message
     -v,--version           Display script version
     -u,--upgrade           Upgrade voidlinux system
     -n,--nonfree           Install Non-free repo
     -i,--input             Install driver input
     -y,--xorg              Install xorg display
     -l,--look              Install cursor, icon & font
     -t,--tools             Install tools utilities
     -z,--zsh               Install zsh shell
     -p,--play              Install codec audio and video
     -b,--build             Install builder program (compiler)
     -a,--archive           Install archive and compression
     -r,--printer           Install cups and printer
     -o,--office            Install office program
     -w,--wine              Install wine emulator
     -d,--design            Install graphics design apps
     -g,--gnome             Install gnome base
     -c,--cinnamon          Install cinnamon base
     -m,--mate              Install mate base
     -x,--xfce              Install xfce base
     -ga,--gnome-apps       Install gnome apps
     -gs,--gnome-service    Install gnome apps
     -ds,--desktop-service  Install desktop services
```

## Example

```
$ sudo voidy --archive

[*] Updating repository `https://alpha.de.repo.voidlinux.org/current/x86_64-repodata' ...
[*] Updating repository `https://alpha.de.repo.voidlinux.org/current/nonfree/x86_64-repodata' ...
Package `atool' already installed.
Package `p7zip' already installed.
Package `zip' already installed.
Package `unzip' already installed.
Package `lz4' already installed.
Package `lzop' already installed.
Package `lzip' already installed.
Package `archiver' already installed.
Package `file-roller' already installed.
```
